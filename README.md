# Innocent

```txt
Innocent; INstall NOte and Configuration of gENToo linux
```

This project is note about general setup on Gentoo Linux (in Linode).

```zsh
# uname -a
Linux <HOSTNAME> 4.9.7-x86_64-linode80 #2 SMP Thu Feb 2 15:43:55 EST 2017 x86_64 Intel(R) Xeon(R) CPU E5-2680 v3 @ 2.50GHz GenuineIntel GNU/Linux
```


## Note(s)

* [server](doc/server.md)

See also [vincent](https://gitlab.com/grauwoelfchen/vincent) for OpenVPN setup.



## License

Copyright (c) 2017 Yasuhiro Asaka

### Notes

This project is distributed as **GNU Free Documentation License**.
(version 1.3)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".

See [LICENSE](LICENSE). (`GFDL-1.3`)
