# Server

## Machine

* Linode 2048 (CPU 1, 2GB Memory)
* Gentoo Linux (gentoo-20170105, with `genkernel`)

```bash
# uname -a
Linux <HOSTNAME> 4.9.7-x86_64-linode80 #2 SMP Thu Feb 2 15:43:55 EST 2017 x86_64 Intel(R) Xeon(R) CPU E5-2680 v3 @ 2.50GHz GenuineIntel GNU/Linux
```


## Preparation

```bash
: update make.conf
# $EDITOR /etc/portage/make.conf
```

```txt
MAKEOPTS="-j2"
ACCEPT_KEYWORDS="~amd64"
LINGUAS="en"

PYTHON_SINGLE_TARGET="python2_7"
PYTHON_TARGETS="python2_7 python3_5"
```

Let's setup `USE` flag as you want.

```
USE="\
bindist \
nls unicode truetype \
ssl \
zlib zip bzip2 \
jpeg gif png svg tiff \
ncurses readline \
-X \
-gtk -gtk3 -gnome -kde -qt -qt4 \
-gpm -arts -berkdb -gdbm \
$USE"
```

```bash
: update portage(s)
# emerge --sync
# dispatch-conf

: (re)set locale
# $EDITOR /etc/locale.gen
en_US.UTF-8 UTF-8

: set hostname
# $EDITOR /etc/conf.d/hostname
# /etc/init.d/hostname restart

# eselect news read all | less

: check timezone
# cat /etc/timezone
UTC
```

### Perl

```bash
: update perl
: https://wiki.gentoo.org/wiki/Project:Perl/How_to_version-bump_dev-perl

: check installed packages and dependencies
# equery l -pob perl-core/\*
# equery l -pob dev-perl/\*
# equery l -pob dev-lang/perl
# equery l -pob virtual/perl*
# emerge -av dev-lang/perl --verbose-conflicts

: fix dependencies
# emerge -av File-Temp Locale-gettext Module-Build SGMLSpm TermReadKey Text-CharWidth Text-Unidecode Text-WrapI18N Unicode-EastAsianWidth XML-Parser libintl-perl perl-CPAN-Meta perl-CPAN-Meta-YAML perl-Data-Dumper perl-ExtUtils-CBuilder perl-ExtUtils-Install perl-ExtUtils-MakeMaker perl-ExtUtils-Manifest perl-ExtUtils-ParseXS perl-File-Spec perl-File-Temp perl-Getopt-Long perl-JSON-PP perl-Module-Metadata perl-Parse-CPAN-Meta perl-Perl-OSType perl-Test-Harness perl-Text-ParseWords perl-version po4a

# emerge -av @preserved-rebuild

: update perl-cleaner
# emerge -av perl-cleaner

# perl-cleaner --all
# perl --version && perl -V | less
```

### Update installed portages

```bash
: update installed portages
: (eudev-3.2.1 and libcap-2.25 can not be installed)
# emerge --update --deep --with-bdeps=y --newuse @world
```


## NTP

```bash
# echo 'net-misc/ntp vim-syntax' >> /etc/portage/package.use/ntp

# emerge -av net-misc/ntp ntp-syntax
# service ntp-client start
# eselect rc add ntp-client default
```


## User

```bash
: create user
# emerge -av sudo
# useradd USER -G wheel -s /bin/bash
# passwd USER

: add `%wheel group`
# $EDITOR /etc/sudoers
```


```bash
: setup for sshd
# su USER

: put public key(s)
$ mkdir ~/.ssh; $EDITOR ~/.ssh/authorized_keys
$ chmod 700 -R ~/.ssh && chmod 600 ~/.ssh/authorized_keys
```


## SSH

```txt
# /etc/ssh/sshd_config
Port <PORT>
AddressFamily any
PrintMotd no
PrintLastLog yes
UsePAM yes
TCPKeepAlive yes
ClientAliveInterval 120
ClientAliveCountMax 2
PermitRootLogin no
PermitEmptyPasswords no
ChallengeResponseAuthentication no
PasswordAuthentication no
PubkeyAuthentication yes
```

```bash
: restart sshd
# service sshd restart
```



## Firewall

Create `iptables.sh` for your rules.

```bash
$ sudo su
# echo 'net-firewall/iptables conntrack nftables' >> /etc/portage/package.use/iptables
# exit
$ sudo emerge -av iptables
```

```bash
: create iptables.sh
$ $EDITOR ~/iptables.sh

: use your iptables script :)
$ sudo sh ./iptables.sh
```

```bash
$ sudo service iptables start
$ sudo eselect rc add iptables default
```


## tools

```bash
$ sudo emerge -av cronie logrotate syslog-ng
$ sudo service cronie start
$ sudo eselect rc add cronie default

$ sudo service syslog-ng start
$ sudo eselect rc add syslog-ng default
```

### Additional tools

Skip if you don't use these tools.

### Monitoring tools

```bash
$ sudo emerge -av htop glances
```

### Monitoring agent (linode's longview)

You can skip also this step, if you don't want to use it.

```bash
# this install script mostly works, but init script fails at boot
$ curl -S https://lv.linode.com/XXXXX-XXXX... | sudo bash
```

Create init-script (openrc-run) by your self.  
See `lib/longview`


### Other tools

```bash
$ sudo emerge -av eix
$ sudo eix-update

$ sudo emerge -av dev-vcs/git app-misc/screen

: if you need it
$ sudo emerge -av mlocate
$ sudo updatedb

: if you want to change shell
$ sudo emerge -av zsh zsh-completions
$ chsh -s /bin/zsh USER
```
