# Docker

```bash
$ sudo emerge -av app-emulation/docker docker-compose
```

The installation `app-emulation/docker` reports following
missing-configurations for linux kernel.

```
* Messages for package app-emulation/docker-17.03.0:

*   CONFIG_MEMCG_SWAP: is required if you wish to limit swap usage of containers
*   CONFIG_MEMCG_SWAP_ENABLED:  is not set when it should be.
*   CONFIG_BLK_DEV_THROTTLING:  is not set when it should be.
*   CONFIG_CFQ_GROUP_IOSCHED:   is not set when it should be.
*   CONFIG_CGROUP_HUGETLB:      is not set when it should be.
*   CONFIG_RT_GROUP_SCHED:      is not set when it should be.
*   CONFIG_IP_VS:       is not set when it should be.
*   CONFIG_IP_VS_PROTO_TCP:     is not set when it should be.
*   CONFIG_IP_VS_PROTO_UDP:     is not set when it should be.
*   CONFIG_IP_VS_NFCT:  is not set when it should be.
*   CONFIG_VXLAN:       is not set when it should be.
*   CONFIG_MEMCG_KMEM: is optional
*   CONFIG_DEVPTS_MULTIPLE_INSTANCES:   is not set when it should be.
```

```bash
$ sudo su
# cd /usr/src/linux

: edit kernel configuration and then
# make menuconfig
# make && make modules_install

: use genkernel
# genkernel all
```

```
$ sudo service docker start
$ sudo eselect rc add docker default
```

```
$ sudo docker info
```

## Group

``zsh
: add your USER to docker group
# usermod -aG docker USER
```

## Logrotate

Create `/etc/logrotate.d/docker`

```txt
/var/log/docker.log {
    missingok
    compress
    rotate 4
    weekly
    delaycompress
    notifempty
    sharedscripts
    copytruncate
}
```
